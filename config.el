;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "Chris Vail"
      user-mail-address "chris_vail@live.ca")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
;; (setq doom-font (font-spec :family "monospace" :size 12 :weight 'semi-light)
;;       doom-variable-pitch-font (font-spec :family "sans" :size 13))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-font (font-spec :family "Liberation Mono" :size 18)
      doom-big-font (font-spec :family "Liberation Mono" :size 28))

(custom-set-faces!
  '(font-lock-comment-face :slant italic)
  '(font-lock-keyword-face :slant italic))

(setq doom-theme 'doom-tomorrow-night)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/org/")

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type t)


;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.

(require 'disable-mouse)
(global-disable-mouse-mode)

(require 'ccls)
(use-package lsp-mode :commands lsp)
(use-package lsp-ui :commands lsp-ui-mode)
(use-package company-lsp :commands company-lsp)

(use-package ccls
  :hook ((c-mode c++-mode) .
         (lambda () (require 'ccls) (lsp))))

(use-package modern-cpp-font-lock
  :ensure t)

;;Projectile default search path
(setq projectile-project-search-path '("~/Repos/" "~/Libraries/"))

;;Auto-complete
(require 'company)
(add-hook 'after-init-hook 'global-company-mode)
(setq company-backends (delete 'company-semantic company-backends))
(define-key c-mode-map [(tab)] 'company-complete)
(define-key c++-mode-map [(tab)] 'company-complete)

;;Adding doxygen auto-complete in emacs
(load "~/.doom.d/doxygen.el")

(global-set-key (kbd "s-k") 'doxygen-insert-function-comment)
(global-set-key (kbd "s-l") 'doxygen-insert-file-comment)
(global-set-key (kbd "s-j") 'comment-region)
(global-set-key (kbd "s-u") 'uncomment-region)

(global-set-key (kbd "<s-left>") #'+fold/next)
(global-set-key (kbd "<s-right>") #'+fold/previous)
(global-set-key (kbd "<s-up>") #'+fold/open)
(global-set-key (kbd "<s-down>") #'+fold/close)
(global-set-key (kbd "<s-C-up>") #'+fold/open-all)
(global-set-key (kbd "<s-C-down>") #'+fold/close-all)
(global-set-key (kbd "s-C-F") #'+fold/toggle)

(global-set-key (kbd "s-c") 'copy-region-as-kill)
(global-set-key (kbd "s-x") 'kill-region)
(global-set-key (kbd "s-v") 'yank)
(global-set-key (kbd "s-k c") 'comment-region)
(global-set-key (kbd "s-k u") 'uncomment-region)

(global-set-key (kbd "s-1") #'+workspace/switch-to-0)
(global-set-key (kbd "s-2") #'+workspace/switch-to-1)
(global-set-key (kbd "s-3") #'+workspace/switch-to-2)
(global-set-key (kbd "s-4") #'+workspace/switch-to-3)
(global-set-key (kbd "s-5") #'+workspace/switch-to-4)

(define-key projectile-mode-map [f5] 'projectile-compile-project)
(define-key global-map [f7] 'first-error)
(define-key global-map [f6] 'previous-error)
(define-key global-map [f8] 'next-error)
(global-set-key (kbd "<s-tab>") 'window-swap-states)
(global-set-key (kbd "<C-tab>") 'other-window)
